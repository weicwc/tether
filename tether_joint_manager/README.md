# tether_joint_manager

Launch a controller to move the tether.

	roslaunch tether_joint_manager guide_rail_manager.launch 

Run the effort controller:

	rostopic pub -1 /tether/guide_rail_joint/command std_msgs/Float64 "data: 20.0"