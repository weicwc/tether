# tether

Package to simulate a tether in Gazebo.

First, run the tether in Gazebo:

	roslaunch tether_gazebo tether.launch

Next, run a joint controller for moving the tether:

	roslaunch tether_joint_manager guide_rail_manager.launch

Finally, move the joint:

	rostopic pub -1 /tether/guide_rail_joint/command std_msgs/Float64 "data: 20.0"